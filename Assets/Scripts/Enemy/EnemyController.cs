﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Animator animator;
    public AudioClip deathClip;
    public bool isAlive = true;

    private float speed = 7.5f;
    private float jump = 2000;
    private float gravity = 100;
    private int currentGravity = -1;
    private int direction = 0;

    private new Rigidbody2D rigidbody2D;
    private new Transform transform;
    private AudioSource audioSource;

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();

        StartCoroutine("WalkRoutine");
    }

    void Update()
    {
        currentGravity = Physics2D.gravity.y > 0 ? 1 : -1;
        
        if (isAlive)
        {
            transform.position += Vector3.right * direction * speed * Time.deltaTime;
        }

        animator.SetInteger("speedX", direction);
        animator.transform.localScale = new Vector3(direction != 0 ? direction : animator.transform.localScale.x, -currentGravity, 1);
    }


    private IEnumerator WalkRoutine()
    {
        while (isAlive)
        {
            yield return new WaitForSeconds(0.25f);

            direction = 1;

            yield return new WaitForSeconds(1f);

            direction = 0;

            yield return new WaitForSeconds(0.25f);

            direction = -1;

            yield return new WaitForSeconds(1f);

            direction = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("TRIGGER ENTER: " + collision.tag);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("COLLISION ENTER: " + collision.gameObject.tag);

        if (isAlive)
        {
            if (collision.gameObject.tag == UnityConstants.Tags.Spike)
            {
                Die();
            }
        }
    }

    private void Die()
    {
        animator.gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
        isAlive = false;
        audioSource.PlayOneShot(deathClip);
    }
}
