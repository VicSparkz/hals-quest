﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToGameScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityConstants.Scenes.Level1);
    }
}
