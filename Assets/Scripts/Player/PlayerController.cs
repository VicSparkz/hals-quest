﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator animator;
    public AudioClip jumpClip;
    public AudioClip deathClip;
    public AudioClip gravityClip;
    public GameScene gameScene;

    private float speed = 10;
    private float jump = 1110;
    private float gravity = 100;
    private int currentGravity = -1;
    private bool gravityChanged = false;
    private bool hasWon = false;

    private new Rigidbody2D rigidbody2D;
    private new Transform transform;
    private AudioSource audioSource;

	void Start ()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        transform = GetComponent<Transform>();
        audioSource = GetComponent<AudioSource>();

        Physics2D.gravity = Vector2.up * gravity * currentGravity;
    }
	
	void Update ()
    {
        if (gravityChanged && Mathf.Abs(rigidbody2D.velocity.y) == 0)
        {
            gravityChanged = false;
        }

        int direction = 0;

        if(!hasWon && !gravityChanged)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                direction = 1;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                direction = -1;
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                currentGravity *= -1;
                Physics2D.gravity = Vector2.up * gravity * currentGravity;
                gravityChanged = true;
                audioSource.PlayOneShot(gravityClip);
            }

            if (Input.GetKeyDown(KeyCode.Space) && Mathf.Abs(rigidbody2D.velocity.y) < 0.5)
            {
                rigidbody2D.AddForce(Vector2.up * jump * -currentGravity);
                audioSource.PlayOneShot(jumpClip);
            }
        }

        transform.position += Vector3.right * direction * speed * Time.deltaTime;

        animator.SetInteger("speedX", direction);
        animator.transform.localScale = new Vector3(direction != 0 ? direction : animator.transform.localScale.x, -currentGravity, 1);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("TRIGGER ENTER: " + collision.tag);

        if(!hasWon)
        {
            if (collision.tag == UnityConstants.Tags.Flag)
            {
                hasWon = true;
                collision.GetComponent<Flag>().Raise();
                gameScene.Win();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("COLLISION ENTER: " + collision.gameObject.tag);

        if (!hasWon)
        {
            if (collision.gameObject.tag == UnityConstants.Tags.Enemy)
            {
                if(collision.gameObject.GetComponent<EnemyController>().isAlive)
                {
                    Die();
                }
            }
            else if (collision.gameObject.tag == UnityConstants.Tags.Spike)
            {
                Die();
            }
            //else if (collision.gameObject.tag == UnityConstants.Tags.Box)
            //{
            //    if(collision.gameObject.transform.position.y > transform.position.y)
            //    {
            //        Die();
            //    }
            //}
        }
    }

    private void Die()
    {
        gameScene.Lose();
        hasWon = true;
        animator.transform.eulerAngles = new Vector3(0, 0, 90);
        audioSource.PlayOneShot(deathClip);
    }
}
