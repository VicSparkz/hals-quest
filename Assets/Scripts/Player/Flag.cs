﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour
{
    public AudioClip raiseClip;

    private AudioSource audioSource;

    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
	}
	
	void Update ()
    {
		
	}

    public void Raise()
    {
        if(GetComponent<Animator>() != null)
        {
            GetComponent<Animator>().SetTrigger("raise");
        }

        audioSource.PlayOneShot(raiseClip);
    }
}
